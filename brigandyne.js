Hooks.once("init", function() {
    // Register sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("brigandyne", ActorSheetBrigandyne, {
        types: ["character"],
        makeDefault: true,
        label: "Brigandyne.SheetCharacter"
    });
    // Preload Handlebars Templates
    return preloadHandlebarsTemplates();

}